import urllib2
import re
from bs4 import BeautifulSoup
url = 'https://bandcamp.com/tag/iceland?sort_field=date'

conn = urllib2.urlopen(url)
html = conn.read()

soup = BeautifulSoup(html)
links = soup.find_all('a')

regex = re.compile('.*album.*')

for tag in links:
	link = tag.get('href',None)
	
	if re.match(regex, str(link)) and link is not None:
		print link
